package cordova.plugin.checkpushmessage;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

/**
 * This class triggers a method to check for notification messgae in back ground and NIM .
 */
public class CheckPushMessage extends CordovaPlugin {

    private static final String TAG="CheckPushMessagePlugin";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("payload")) {
            Log.d(TAG,"=:Payload Received:=");
            //to capture all key,value pair received from push notifications only
            if (cordova.getActivity().getIntent().getExtras() != null) {
                if(cordova.getActivity().getIntent().getExtras().keySet().contains("google.message_id")){
                Log.d(TAG,"=:Parsing Payload:=");
                //create a Json Object to send to ionic code
                JSONObject pushObject=new JSONObject();
                    for (String key : cordova.getActivity().getIntent().getExtras().keySet()) {
                        Object value = cordova.getActivity().getIntent().getExtras().get(key);
                        Log.d(TAG,"{Key :" + key +","+ "Value: " + value+"}");
                        pushObject.put(key,value);
                        //to clear the key from the intent once its values is stored in json object; avoiding same values to send again
                        cordova.getActivity().getIntent().removeExtra(key);
                    }
                         //once all the key values pairs from push notification is stotred in pushObject we send it to ionic code
                        callbackContext.success(pushObject);
                }
            }
        }
        return true;
    }
}

